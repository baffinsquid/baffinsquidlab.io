---
layout: page
title: About
permalink: /about/
---

Hi there!

I'm BaffinSquid🦑, ~~a knowledge-hungry cephalopod~~ a postgraduate who works in the field of [EPR spectroscopy](https://en.wikipedia.org/wiki/Electron_paramagnetic_resonance).

This site is devoted to my interests in computer graphics, open-source software, papercrafts and, last but not least, astronomy. 

I make printable models of celestial bodies that are used as a teaching resource. The graphic on each 'globe' is made from real mapping data. Patterns are made on GNU/Linux using open-source software Blender, GIMP and, to a lesser degree, Hugin.
