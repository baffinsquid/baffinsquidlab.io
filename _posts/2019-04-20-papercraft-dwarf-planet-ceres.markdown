---
layout: post
title:  "Papercraft dwarf planet Ceres with topographic features"
date:   2019-04-20 18:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-04-20/thumbnail.jpg
gallery:
  - name: Papercraft dwarf planet Ceres photo 1
    file: /files/2019-04-20/Paper_Ceres_1.jpg
  - name: Papercraft dwarf planet Ceres photo 2
    file: /files/2019-04-20/Paper_Ceres_2.jpg
  - name: Papercraft dwarf planet Ceres photo 3
    file: /files/2019-04-20/Paper_Ceres_3.jpg
  - name: Papercraft dwarf planet Ceres pattern
    file: /files/2019-04-20/Paper_Ceres_Pattern.png
---

{% include auto-gallery.html %}

### Download
* [Ceres pattern](/files/2019-04-20/Paper_Ceres_v2.pdf) (5.6 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-04-20 - Initial release.

### Dimentions
* Size: 9,0 cm x 9,0 cm x 9,0 cm

### Supplementary Data
Modified map with coordinate grid that account distortion around polar regions:
* [Ceres map](/files/2019-04-20/Ceres_Map.png) (2.8 MiB PNG file)

### Description
Papercraft icosahedral model of the dwarf planet [Ceres][wiki-ceres] (1 Ceres) with large impact craters labeled on a topographic map. Ceres was the first asteroid to be discovered (in 1801) and it is the largest object in the asteroid belt (with almost one-third of the belt's total mass). Ceres was studied by [NASA's Dawn][wiki-nasa-dawn] automatic explorer between 2015 and 2018.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
Ceres map by NASA/JPL-Caltech/UCLA/MPS/DLR/IDA: [PIA19606: Topographic Ceres Map With Crater Names](https://photojournal.jpl.nasa.gov/catalog/PIA19606)

[wiki-nasa-dawn]: https://en.wikipedia.org/wiki/Dawn_(spacecraft)
[wiki-ceres]:  https://en.wikipedia.org/wiki/Ceres_(dwarf_planet)
