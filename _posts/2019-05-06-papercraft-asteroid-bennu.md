---
layout: post
title:  "Papercraft asteroid Bennu with elevation map"
date:   2019-05-06 07:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-05-06/Thumbnail75x50.jpg
gallery:
  - name: Papercraft asteroid Bennu photo 1
    file: /files/2019-05-06/Paper_Bennu_1.jpg
  - name: Papercraft asteroid Bennu photo 2
    file: /files/2019-05-06/Paper_Bennu_2.jpg
  - name: Papercraft asteroid Bennu photo 3
    file: /files/2019-05-06/Paper_Bennu_3.jpg
  - name: Papercraft asteroid Bennu pattern
    file: /files/2019-05-06/Paper_Bennu_Pattern.png
---

{% include auto-gallery.html %}

### Download
* [Bennu pattern](/files/2019-05-06/Paper_Bennu_v2.pdf) (5.2 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-05-06 - Initial release.

### Dimentions
* Size: 9,0 cm x 9,0 cm x 9,0 cm
* Scale: 1/3000

### Description
Papercraft polyhedral model of asteroid [Bennu][wiki-bennu] (101955 Bennu, 1999 RQ36) with the global elevation map. As of 2019 Bennu is being explored by the [OSIRIS-REx][wiki-osiris-rex] probe. Gradient from blue to red shows elevation above the geopotential from 0m (blue) to 70m (red). No data about polar regions at >70 degrees seems to be available.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Map of Bennu by NASA/Goddard/University of Arizona: [Global Elevation Map of Asteroid Bennu](https://www.asteroidmission.org/?attachment_id=15897)

[wiki-osiris-rex]: https://en.wikipedia.org/wiki/OSIRIS-REx
[wiki-bennu]:  https://en.wikipedia.org/wiki/101955_Bennu

