---
layout: post
title:  "Saturn's moon Titan with named geological features"
date:   2019-05-01 16:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-05-01/Thumbnail_75x50.jpg
gallery:
  - name: Saturn's moon Titan photo 1
    file: /files/2019-05-01/Saturns_Moon_Titan_1.jpg
  - name: Saturn's moon Titan photo 2
    file: /files/2019-05-01/Saturns_Moon_Titan_2.jpg
  - name: Saturn's moon Titan photo 3
    file: /files/2019-05-01/Saturns_Moon_Titan_3.jpg
  - name: Saturn's moon Titan pattern
    file: /files/2019-05-01/Paper_Titan_Pattern.png
---

{% include auto-gallery.html %}

### Download
* [Titan pattern](/files/2019-05-01/Paper_Titan_v2.pdf) (2.3 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-05-01 - Initial release.

### Dimentions
* Size: 8,0 cm x 8,0 cm x 8,0 cm

### Supplementary Data
Modified map with coordinate grid that account distortion around polar regions:
* [Titan map](/files/2019-05-01/Titan_Map.png) (1.9 MiB PNG file)

### Description
[Titan][wiki-titan] is the largest moon of Saturn and the second-largest natural satellite in the Solar System (after Jupiter's moon Ganymede) that is also larger than the planet Mercury.

It's dense opaque atmosphere, that consists largely of nitrogen, prevents imaging of the Titan's surface in visible light. It was not until the [Cassini-Huygens][wiki-cassini-huygens] probe orbited Saturn between 2004 and 2017 that the first direct images of Titan's surface were obtained in the infrared spectrum. Due to that [features on the surface of Titan][wiki-titan-features] are distinguished by their contrast in brightness or darkness (albedo) in the specific wavelengths.

[Huygens probe][wiki-huygens] that was a part of the Cassini-Huygens mission landed on Titan on January 14, 2005. Up to this day Titan is the most distant body from Earth to have a robotic probe land on its surface.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Titan map by NASA/JPL-Caltech/Univ. Arizona: [PIA22770: Titan Mosaic: The Surface Under the Haze](https://photojournal.jpl.nasa.gov/catalog/PIA22770)

[wiki-cassini-huygens]: https://en.wikipedia.org/wiki/Cassini%E2%80%93Huygens
[wiki-huygens]: https://en.wikipedia.org/wiki/Huygens_(spacecraft)
[wiki-titan]: https://en.wikipedia.org/wiki/Titan_(moon)
[wiki-titan-features]: https://en.wikipedia.org/wiki/List_of_geological_features_on_Titan

