---
layout: post
title:  "Saturn's icy moons: Enceladus and Iapetus"
date:   2019-05-10 21:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-05-10/Thumbnail75x50.jpg
gallery:
  - name: Papercraft Enceladus and Iapetus photo 1
    file: /files/2019-05-10/Paper_Enceladus_and_Iapetus_1.jpg
  - name: Papercraft Enceladus and Iapetus photo 2
    file: /files/2019-05-10/Paper_Enceladus_and_Iapetus_2.jpg
  - name: Papercraft Enceladus and Iapetus photo 3
    file: /files/2019-05-10/Paper_Enceladus_and_Iapetus_3.jpg
  - name: Papercraft Enceladus pattern
    file: /files/2019-05-10/Paper_Enceladus_Pattern.png
  - name: Papercraft Iapetus pattern
    file: /files/2019-05-10/Paper_Iapetus_Pattern.png
---

{% include auto-gallery.html width="20" %}

### Download
* [Enceladus pattern](/files/2019-05-10/Paper_Enceladus_v2.pdf) (11.3 MiB PDF file)
* [Iapetus pattern](/files/2019-05-10/Paper_Iapetus_v2.pdf) (9.6 MiB PDF file)

### History
* 2019-05-10 - Initial release.

### Dimentions
* Size: 9,0 cm x 9,0 cm x 9,0 cm

### Supplementary Data
Modified map with coordinate grid that account distortion around polar regions:
* [Enceladus map](/files/2019-05-10/Enceladus_Map.png) (13.0 MiB PNG file)
* [Iapetus map](/files/2019-05-10/Iapetus_Map.png) (8.2 MiB PNG file)

### Description
[Enceladus][wiki-enceladus] is one of the smallest of [Saturn's moons][wiki-saturns-moons] that is spherical in shape, however it is the smallest celestial body in the Solar System that is geologically active. Surface of Enceladus is fractured and covered by [systems of geyser gorges][wiki-features-of-enceladus] among other features, that are called "tiger stripes". This grooves emit jets of water, ice and dust into space. Some of the gorges are about 130 km long and the jets there [form distinct plumes around this moon][apod-enceladus].

[Iapetus][wiki-iapetus] is the third-largest of Saturn's moons known for its unusual two-toned surface. It can be observed only on the west side of Saturn and never on the east doe to the fact that one side of Iapetus is darker than the other. With the difference in colouring between the two Iapetian hemispheres so striking the pattern of coloration is analogous to a spherical yin-yang symbol or the two sections of a tennis ball.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Maps of Enceladus and Iapetus by NASA/JPL-Caltech/Space Science Institute/Lunar and Planetary Institute.
* [Color Maps of Enceladus - 2014](https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA18435)
* [Color Maps of Iapetus - 2014](https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA18436)

### See also
* [Enceladus — First Globe Ever](https://rightbasicbuilding.com/enceladus-first-globe-ever/) by Chuck Clark

[apod-enceladus]: https://apod.nasa.gov/apod/ap170416.html
[wiki-saturns-moons]: https://en.wikipedia.org/wiki/Moons_of_Saturn
[wiki-features-of-enceladus]: https://en.wikipedia.org/wiki/List_of_geological_features_on_Enceladus
[wiki-enceladus]: https://en.wikipedia.org/wiki/Enceladus
[wiki-iapetus]:  https://en.wikipedia.org/wiki/Iapetus_(moon)

