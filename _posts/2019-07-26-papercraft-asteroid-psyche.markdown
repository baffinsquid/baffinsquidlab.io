---
layout: post
title:  "Papercraft asteroid 16 Psyche"
date:   2019-07-26 00:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-07-26/Thumbnail75x50.jpg
gallery:
  - name: Papercraft asteroid Psyche photo 1
    file: /files/2019-07-26/16_Psyche_Photo_1.jpg
  - name: Papercraft asteroid Psyche photo 2
    file: /files/2019-07-26/16_Psyche_Photo_2.jpg
  - name: Papercraft asteroid Psyche photo 3
    file: /files/2019-07-26/16_Psyche_Photo_3.jpg
  - name: Papercraft asteroid Psyche pattern page 1
    file: /files/2019-07-26/16_Psyche_Pattern_p1.png
  - name: Papercraft asteroid Psyche pattern page 2
    file: /files/2019-07-26/16_Psyche_Pattern_p2.png
---

{% include auto-gallery.html width="20" %}

### Download
* [16 Psyche pattern](/files/2019-07-26/16_Psyche_Pattern_v2.pdf) (156.3 KiB PDF file)

### History
* 2019-07-28 - Larger model. Rearranged islands.
* 2019-07-26 - Initial release.

### Dimensions
* Size: 21,0 cm x 19,0 cm x 11,0 cm
* Scale: 1/1,400,000

### Description
[Psyche][wiki-psyche] (16 Psyche) is the most massive metallic M-type asteroid and one of the most massive asteroids in the asteroid belt. Its diameter is over 200 km and it consists purely of iron-nickel alloy. Therefore is thought to be the iron core of a protoplanet that failed to form during early stages of the Solar system.

In 2022 [Psyche orbiter mission][wiki-psyche-orbiter] mission will launch and arrive to the asteroid in four years to perform 21 months of exploration between 2026 and 2027.

Current shape data is based on the radar astronomy of the [Arecibo Observatory][wiki-arecibo] and shows two distinct features that are most likely to be large craters.

Made with Blender and Paper Model plug-in: [Export-Paper-Model-from-Blender](https://github.com/addam/Export-Paper-Model-from-Blender)

### Tools
* [Blender](https://www.blender.org)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
Model for the asteroid has been created using radar data from [Shepard et al. 2017](https://doi.org/10.1016/j.icarus.2016.08.011).

### See also
* [Concept model of asteroid Psyche](https://www.thingiverse.com/thing:2373526) - A model for 3D print.

[wiki-psyche]: https://en.wikipedia.org/wiki/16_Psyche
[wiki-psyche-orbiter]: https://en.wikipedia.org/wiki/Psyche_(spacecraft)
[wiki-arecibo]: https://en.wikipedia.org/wiki/Arecibo_Observatory

