---
layout: post
title:  "Papercraft global geologic map of Titan"
date:   2020-01-14 12:00:00 +0200
categories: papercraft space
thumbnail: /files/2020-01-14/Thumbnail_75x50.jpg
gallery:
  - name: Geologic map of Titan photo 1
    file: /files/2020-01-14/Geologic_map_of_Titan_1.jpg
  - name: Geologic map of Titan photo 2
    file: /files/2020-01-14/Geologic_map_of_Titan_2.jpg
  - name: Geologic map of Titan photo 3
    file: /files/2020-01-14/Geologic_map_of_Titan_3.jpg
  - name: Geologic map of Titan photo 4
    file: /files/2020-01-14/Geologic_map_of_Titan_4.jpg
  - name: Geologic map of Titan photo 5
    file: /files/2020-01-14/Geologic_map_of_Titan_5.jpg
---

{% include auto-gallery.html width="20" %}

### Download
* [Titan pattern](/files/2020-01-14/Paper_Geologic_Titan_v1.pdf) (6.0 MiB PDF file)

### History
* 2020-01-14 - Initial release

### Dimentions
* Size: 15,0 cm x 15,0 cm x 15,0 cm

### Description
[Titan][wiki-titan] is the second-largest natural satellite in the Solar System (after Jupiter's moon Ganymede) that is also larger than the planet Mercury.

It's dense opaque atmosphere, that consists largely of nitrogen, prevents imaging of the Titan's surface in visible light. It was not until the [Cassini-Huygens][wiki-cassini-huygens] probe orbited Saturn between 2004 and 2017 that the first direct images of Titan's surface were obtained in the infrared spectrum. Due to that [features on the surface of Titan][wiki-titan-features] are distinguished by their contrast in brightness or darkness (albedo) in the specific wavelengths.

[Huygens probe][wiki-huygens] that was a part of the Cassini-Huygens mission landed on Titan on January 14, 2005. Up to this day Titan is the most distant body from Earth to have a robotic probe land on its surface.

In 2019 NASA's JPL team [released the first global geologic map of Titan][map-release] which is based on images from NASA's Cassini mission, which orbited Saturn from 2004 to 2017.

#### Legend
![Geologic map legend](/files/2020-01-14/Map_Legend.png)

#### Titan's coordinate system
Titan is [tidally locked][wiki-tidal-lock] in synchronous rotation with Saturn, and permanently shows one face to the planet, so Titan's "day" is equal to its orbit period. Because of this, there is a sub-Saturnian point on its surface, from which the planet would always appear to hang directly overhead. Longitudes on Titan are measured westward, starting from the meridian passing through this point:
* 0&deg; &mdash; prime meridian, facing Saturn;
* 90&deg; &mdash; leading hemisphere;
* 180&deg; &mdash; antimeridian, the far side of Titan, hemisphere that always faces away from Saturn;
* 270&deg; &mdash; trailing hemisphere.

![Diagram of Titan's longitudes](/files/2020-01-14/Titan_Longitudes.png)

### Tools
* [Blender](https://www.blender.org)
* [Export Paper Model](https://addam.github.io/Export-Paper-Model-from-Blender) add-on for Blender
* [GIMP2](https://www.gimp.org)
* [OctaGlobe scripts](https://gitlab.com/Postrediori/OctaGlobe)

### Credits
* Global Geologic Map of Titan by NASA/JPL-Caltech/ASU: Lopes, Rosaly MC, et al. [A global geomorphologic map of Saturn's moon Titan](https://www.nature.com/articles/s41550-019-0917-6). Nature Astronomy (2019): 1-6.
* [OctaGlobe projection](http://mason.gmu.edu/~bklinger/SectorGlobe/sectorglobe.html) by Barry A. Klinger.
* [OctaGlobe scripts](https://github.com/Postrediori/OctaGlobe) by [GitHub/Postrediori](https://github.com/Postrediori).

[map-release]: https://www.nasa.gov/feature/jpl/the-first-global-geologic-map-of-titan-completed
[wiki-cassini-huygens]: https://en.wikipedia.org/wiki/Cassini%E2%80%93Huygens
[wiki-huygens]: https://en.wikipedia.org/wiki/Huygens_(spacecraft)
[wiki-titan]: https://en.wikipedia.org/wiki/Titan_(moon)
[wiki-titan-features]: https://en.wikipedia.org/wiki/List_of_geological_features_on_Titan
[wiki-tidal-lock]: https://en.m.wikipedia.org/wiki/Tidal_locking
