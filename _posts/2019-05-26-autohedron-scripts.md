---
layout: post
title:  "Generation of foldable polyhedrons"
date:   2019-05-26 16:00:00 +0200
tags:   papercraft space milky-way
categories: papercraft scripts
thumbnail: /files/2019-05-26/Thumbnail80x50.png
---

## Download

* [Source code](https://gitlab.com/baffinsquid/Autohedron/-/archive/master/Autohedron-master.tar.gz)
* [Git source code repository](https://gitlab.com/baffinsquid/Autohedron)

## Description

Scripts to convert equirectangular map projection to foldable polyhedrons:
* [Cube](https://en.wikipedia.org/wiki/Cube_mapping)
* [Rhombicuboctahedron](https://en.wikipedia.org/wiki/Rhombicuboctahedron)

This can be used for maps to create polyhedral models of the Earth and other celestial bodies, generate skybox or create a foldable panoram as [shown by Philo](http://www.philohome.com/rhombicuboctahedron/rhombicuboctahedron.htm). This scripts are implemented for the papercraft [polyhedral celestial bodies in my blog](https://baffinsquid.gitlab.io/).

![Cobemap fold](/files/2019-05-26/Cubemap.png)
![Polyhedron fold](/files/2019-05-26/Polyhedron.png)

## Required tools

* [Hugin](http://hugin.sourceforge.net/) - Panorama tool. `nona` console utility is used for extracting an individual face out of a equirectangular map or panorama image.
* [ImageMagick](https://imagemagick.org/index.php) - Console tool for stitching individual faces together.

Installation instructions for Ubuntu or Debian.

```
apt-get install imagemagick hugin
```

## Usage

### Cubemap
Script `cubemap.sh` generates a full cubemap file `Image-Cubemap.png` and individual cube faces `Image-Up.png`, etc.

```
./cubemap.sh Image.png
```

### Polyhedron
Script `polyhedron.sh` generates a map `Image-Philosphere.png` and its two parts in `Image-S1.png` and `Image-S2.png`. The designations of parts and polyhedron faces are taken from [Philo's script](http://www.philohome.com/rhombicuboctahedron/rhombicuboctahedron.htm).

```
./polyhedron.sh Image.png
```

This script uses files `mask-512-up.png` and `mask-512-down.png` for manipulating triangular faces. Don't remove them.

## Links
### Scripts
* [Rhombicuboctahedron by Philo](http://www.philohome.com/rhombicuboctahedron/rhombicuboctahedron.htm) - Script data for **nona** panorama tool.
* [Irregular Truncated Icosahedron by Max and Fabian Hombsch](http://www.hombsch.de/hedron/) - Bash scripts.
* [How do I make cubemaps with Hugin?](https://photo.stackexchange.com/a/61654) - a Stackexchange page regarding the question.

### Software
* [Map-Projections by Justin Kunimune](https://jkunimune15.github.io/Map-Projections/) - Java application that is capable of generating a plethora of projections.
* [PTGui](https://www.ptgui.com/) - Proprietary program capable of generating a polyhedron out of a map (designated as **philosphere**).

## Todo
* Icosahedron, Fuller projection.
* BAT script for Windows.
* Python package.
