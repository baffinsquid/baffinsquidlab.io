---
layout: post
title:  "Globe of Jupiter's volcanic moon Io"
date:   2020-03-31 14:00:00 +0200
categories: papercraft space
thumbnail: /files/2020-03-31/Thumbnail_75x50.jpg
gallery:
  - name: Globe of Io photo 1
    file: /files/2020-03-31/Io_Photo_1.jpg
  - name: Globe of Io photo 2
    file: /files/2020-03-31/Io_Photo_2.jpg
  - name: Globe of Io photo 3
    file: /files/2020-03-31/Io_Photo_3.jpg
---

{% include auto-gallery.html width="33" %}

### Download
* [Io Globe pattern](/files/2020-03-31/Io_Globe_v1.pdf) (7.23 MiB PDF file)

### History
* 2020-03-31 - Initial release

### Dimentions
* Size: 15,0 cm x 15,0 cm x 15,0 cm

### Description
With over 400 active volcanoes, Jupiter's moon [Io][wiki-io] is the most volcanically active body in the Solar System. Volcanoes erupt massive volumes of silicate lava,
sulphur and sulphur dioxide, constantly changing Io's appearance. The basemap of Io was produced by combining the best images from both the [Voyager 1][wiki-voyager-1] and [Galileo][wiki-galileo] Missions.

Unlike most satellites in the outer Solar System, which are mostly composed of water ice, Io is primarily composed of silicate rock surrounding a molten iron or iron sulfide core. Most of Io's surface is composed of extensive plains coated with sulfur and sulfur dioxide frost. Several volcanoes produce plumes of sulfur and sulfur dioxide that climb as high as 500 km (300 mi) above the surface. Io's surface is also dotted with more than 100 mountains that have been uplifted by extensive compression at the base of Io's silicate crust.

Io's volcanism is responsible for many of its unique features. Its volcanic plumes and lava flows produce large surface changes and paint the surface in various subtle shades of yellow, red, white, black, and green, largely due to allotropes and compounds of sulfur. Numerous extensive lava flows, several more than 500 km (300 mi) in length, also mark the surface.

### Tools
* [Blender](https://www.blender.org)
* [Export Paper Model](https://addam.github.io/Export-Paper-Model-from-Blender) add-on for Blender
* [GIMP2](https://www.gimp.org)
* [OctaGlobe projection](http://mason.gmu.edu/~bklinger/SectorGlobe/sectorglobe.html) by Barry A. Klinger.
* [OctaGlobe scripts](https://gitlab.com/Postrediori/OctaGlobe) at GitLab by [Postrediori](https://gitlab.com/Postrediori).

### Credits
* [Io Galileo SSI / Voyager Color Merged Global Mosaic 1km](https://astrogeology.usgs.gov/search/map/Io/Voyager-Galileo/Io_GalileoSSI-Voyager_Global_Mosaic_ClrMerge_1km). USGS Astrogeology Science Center. 2019.

[wiki-io]: https://en.wikipedia.org/wiki/Io_(moon)
[wiki-voyager-1]: https://en.wikipedia.org/wiki/Voyager_1
[wiki-galileo]: https://en.wikipedia.org/wiki/Galileo_(spacecraft)
