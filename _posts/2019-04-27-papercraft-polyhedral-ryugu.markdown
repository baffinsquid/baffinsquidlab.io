---
layout: post
title:  "Papercraft polyhedral asteroid Ryugu"
date:   2019-04-27 20:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-04-27/thumbnail.jpg
gallery:
  - name: Papercraft polyhedral asteroid Ryugu photo 1
    file: /files/2019-04-27/Paper_Ryugu_Polyhedron_1.jpg
  - name: Papercraft polyhedral asteroid Ryugu photo 2
    file: /files/2019-04-27/Paper_Ryugu_Polyhedron_2.JPG
  - name: Papercraft polyhedral asteroid Ryugu photo 3
    file: /files/2019-04-27/Paper_Ryugu_Polyhedron_3.JPG
  - name: Papercraft polyhedral asteroid Ryugu English pattern
    file: /files/2019-04-27/Paper_Ryugu_Polyhedron_English_Pattern_v3.png
  - name: Papercraft polyhedral asteroid Ryugu Japanese pattern
    file: /files/2019-04-27/Paper_Ryugu_Polyhedron_Japanese_Pattern_v3.png
---

{% include auto-gallery.html width="20" %}

### Download
* [Place names in English](/files/2019-04-27/Paper_Ryugu_Polyhedron_English_v3.pdf) (3.5 MiB PDF file)
* [Place names in Japanese](/files/2019-04-27/Paper_Ryugu_Polyhedron_Japanese_v3.pdf) (3.8 MiB PDF file)

### History
* 2019-12-22 - Update Ryugu map. Add 2nd touchdown operation site marking and name.
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-04-27 - Initial release.

### Dimentions
* Size: 8,0 cm x 8,0 cm x 8,0 cm

### Supplementary Data
Modified maps that account distortion around polar regions:
* [Map projection with English labels](/files/2019-04-27/Ryugu_Map_English_v3.png) (1.1 MiB PNG file)
* [Map projection with Japanese labels](/files/2019-04-27/Ryugu_Map_Japanese_v3.png) (1.3 MiB PNG file)

### Description
On April 4, 2019 space probe Hayabusa2 [fired a 2kg copper mass](http://www.hayabusa2.jaxa.jp/en/topics/20190424e_CRA2_Schedule/) (Small Carry-on Impactor, abbreviated as SCI) to collide with the surface of asteroid Ryugu. On April 23-25, 2019 the probe observed the impact area.

Papercraft polyhedral model of asteroid [Ryugu][wiki-ryugu] (162173 Ryugu, 1999 JU3) with the place names that were chosen by the [Hayabusa2][wiki-hayabusa2] team.

### Tools
* [Blender](https://www.blender.org) and [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* [GIMP2](https://www.gimp.org/)
* [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Ryugu map by JAXA, University of Tokyo, Kochi University, Rikkyo University, Nagoya University, Chiba Institute of Technology, Meiji University, University of Aizu, AIST.
* [Source for the Japanese version](http://www.hayabusa2.jaxa.jp/topics/20190403_SCI_Schedule)
* [Source for the English version](http://www.hayabusa2.jaxa.jp/en/topics/20190403e_SCI_Schedule)

[wiki-hayabusa2]: https://en.wikipedia.org/wiki/Hayabusa2
[wiki-ryugu]:  https://en.wikipedia.org/wiki/162173_Ryugu
 
