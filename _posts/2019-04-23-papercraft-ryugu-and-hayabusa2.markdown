---
layout: post
title:  "Papercraft asteroid Ryugu and Hayabusa2"
date:   2019-04-23 12:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-04-23/thumbnail.jpg
gallery:
  - name: Papercraft asteroid Ryugu and Hayabusa2 photo 1
    file: /files/2019-04-23/Paper_Ryugu_and_Hayabusa2_1.jpg
  - name: Papercraft asteroid Ryugu pattern
    file: /files/2019-04-23/Paper_Ryugu_Pattern.png
---

{% include auto-gallery.html %}

### Download
* [Ryugu pattern](/files/2019-04-23/Paper_Ryugu_v5.pdf) (2.2 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-04-23 - Initial release.

### Dimentions
* Size: 9,0 cm x 9,0 cm x 9,0 cm
* Scale: 1/11,000

### Description
On February 22, 2019 automatic explorer [Hayabusa2][wiki-hayabusa2] made a successful touchdown of the asteroid [Ryugu][wiki-ryugu] (162173 Ryugu, 1999 JU3).

Asteroid Ryugu is approximated by a polyhedron called [rhombicuboctahedron][wiki-polyhedron].

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Ryugu map by JAXA, University of Tokyo, Kochi University, Rikkyo University, Nagoya University, Chiba Institute of Technology, Meiji University, University of Aizu, AIST.
* [Images used in the paper by Sugita et al. (2019).](http://www.darts.isas.jaxa.jp/pub/hayabusa2/paper/Sugita_2019/)

Hayabusa2 cut-out by NASA.
* [Spacecraft Icons](https://science.nasa.gov/toolkits/spacecraft-icons)

[wiki-hayabusa2]: https://en.wikipedia.org/wiki/Hayabusa2
[wiki-ryugu]:  https://en.wikipedia.org/wiki/162173_Ryugu
[wiki-polyhedron]: https://en.wikipedia.org/wiki/Rhombicuboctahedron
