---
layout: post
title:  "Asteroid Ryugu Sector Globe"
date:   2020-04-01 12:00:00 +0200
categories: papercraft space
thumbnail: /files/2020-04-01/Thumbnail_75x50.jpg
gallery:
  - name: Globe of Io photo 1
    file: /files/2020-04-01/Ryugu_SectorGlobe_1.jpg
  - name: Globe of Io photo 2
    file: /files/2020-04-01/Ryugu_SectorGlobe_2.jpg
  - name: Globe of Io photo 3
    file: /files/2020-04-01/Ryugu_SectorGlobe_3.jpg
  - name: Globe of Io photo 4
    file: /files/2020-04-01/Ryugu_SectorGlobe_4.jpg
  - name: Globe of Io photo 5
    file: /files/2020-04-01/Ryugu_SectorGlobe_5.jpg
---

{% include auto-gallery.html width="20" %}

### Download
* [Ryugu sector globe pattern (English)](/files/2020-04-01/Ryugu_SectorGlobe_English_v1.pdf) (4.78 MiB PDF file)
* [Ryugu sector globe pattern (Japanese)](/files/2020-04-01/Ryugu_SectorGlobe_Japanese_v1.pdf) (4.50 MiB PDF file)

### History
* 2020-04-01 - Initial release

### Dimentions
* Size: 15,0 cm x 15,0 cm x 15,0 cm

### Description
Papercraft model of asteroid [Ryugu][wiki-ryugu] (162173 Ryugu, 1999 JU3) was visited by space probe [Hayabusa2][wiki-hayabusa2] between June 2018 and November 2019.

There are two localized versions of the same pattern as they are represented on the [JAXA site](http://www.hayabusa2.jaxa.jp).

The polyhedral projection is eight-sided sector globe that resembles asteroid's "diamond" shape.

### Tools
* [Blender](https://www.blender.org)
* [Export Paper Model](https://addam.github.io/Export-Paper-Model-from-Blender) add-on for Blender
* [GIMP2](https://www.gimp.org)
* [OctaGlobe projection](http://mason.gmu.edu/~bklinger/SectorGlobe/sectorglobe.html) by Barry A. Klinger.
* [OctaGlobe scripts](https://gitlab.com/Postrediori/OctaGlobe) at GitLab by [Postrediori](https://gitlab.com/Postrediori).

### Credits
Ryugu map by JAXA, University of Tokyo, Kochi University, Rikkyo University, Nagoya University, Chiba Institute of Technology, Meiji University, University of Aizu, AIST.
* [Source for the Japanese version](http://www.hayabusa2.jaxa.jp/topics/20190403_SCI_Schedule)
* [Source for the English version](http://www.hayabusa2.jaxa.jp/en/topics/20190403e_SCI_Schedule)

[wiki-hayabusa2]: https://en.wikipedia.org/wiki/Hayabusa2
[wiki-ryugu]:  https://en.wikipedia.org/wiki/162173_Ryugu
