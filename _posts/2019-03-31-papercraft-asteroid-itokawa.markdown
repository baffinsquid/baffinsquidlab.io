---
layout: post
title:  "Papercraft asteroid Itokawa"
date:   2019-03-31 10:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-03-31/thumbnail.jpg
gallery:
  - name: Papercraft asteroid Itokawa photo 1
    file: /files/2019-03-31/Paper_Itokawa_1.jpg
  - name: Papercraft asteroid Itokawa photo 2
    file: /files/2019-03-31/Paper_Itokawa_2.jpg
  - name: Papercraft asteroid Itokawa pattern
    file: /files/2019-03-31/Paper_Itokawa_Pattern.png
---

{% include auto-gallery.html %}

### Download
* [Itokawa pattern](/files/2019-03-31/Paper_Itokawa_v5.pdf) (2.1 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-04-17 - Minor pattern update.
* 2019-03-31 - Initial release.

### Dimentions
* Size: 12,0 cm x 5,0 cm x 5,0 cm
* Scale: 1/5000

### Description
[Hayabusa][wiki-hayabusa] mission was launched in 2003. In 2005 asteroid [Itokawa][wiki-itokawa] was explored by the vehicle which collected particles of dust from the celestial body. The samples were returned to the Earth in 2010.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
Surface texture of Itokawa
* Stooke. P., Stooke Small Bodies Maps V2.0. MULTI-SA-MULTI-6-STOOKEMAPS-V2.0. NASA Planetary Data System, 2012. [Available online](https://sbnarchive.psi.edu/pds3/multi_mission/MULTI_SA_MULTI_6_STOOKEMAPS_V2_0/document/aamapdesc.html#itokawa)
* Gaskell, R., Saito, J., Ishiguro, M., Kubota, T., Hashimoto, T., Hirata, N., Abe, S., Barnouin-Jha, O., and Scheeres, D., Gaskell Itokawa Shape Model V1.0. HAY-A-AMICA-5-ITOKAWASHAPE-V1.0. NASA Planetary Data System, 2008. [Available online](https://sbn.psi.edu/pds/resource/itokawashape.html)
* [Hirata et al. (2009)](https://doi.org/10.1016/j.icarus.2008.10.027)

* Hayabusa mission page by JAXA: [www.hayabusa.isas.jaxa.jp](http://www.hayabusa.isas.jaxa.jp/)
* Hayabusa cut-out by NASA: [Spacecraft Icons](https://science.nasa.gov/toolkits/spacecraft-icons)

### See also
* [Itokawa](https://rightbasicbuilding.com/asteroid-maps/lpsc-handouts/itokawa/) and [Itokawa Cut ‘n’ Fold](https://rightbasicbuilding.com/asteroid-maps/itokawa-cut-n-fold/) by Chuck Clark

[wiki-hayabusa]: https://en.wikipedia.org/wiki/Hayabusa
[wiki-itokawa]:  https://en.wikipedia.org/wiki/25143_Itokawa
