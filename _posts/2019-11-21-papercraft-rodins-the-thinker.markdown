---
layout: post
title:  "Papercraft Rodin's The Thinker"
date:   2019-11-21 13:00:00 +0200
categories: papercraft art
thumbnail: /files/2019-11-21/Thumbnail75x50.jpg
gallery:
  - name: Rodin's The Thinker photo 1
    file: /files/2019-11-21/Rodins_The_Thinker_Photo_1.jpg
  - name: Rodin's The Thinker photo 2
    file: /files/2019-11-21/Rodins_The_Thinker_Photo_2.jpg
  - name: Rodin's The Thinker photo 3
    file: /files/2019-11-21/Rodins_The_Thinker_Photo_3.jpg
  - name: Rodin's The Thinker photo 4
    file: /files/2019-11-21/Rodins_The_Thinker_Photo_4.jpg
  - name: Rodin's The Thinker photo 5
    file: /files/2019-11-21/Rodins_The_Thinker_Photo_5.jpg
---

{% include auto-gallery.html width="20" %}

### Download
* [Rodin's The Thinker pattern](/files/2019-11-21/Rodins_The_Thinker_Pattern_v1.pdf) (287.6 KiB KiB PDF file)

### History
* 2019-11-21 - Initial release.

### Dimensions
Size: 29,0 cm x 20,0 cm x 15,0 cm

### Description
Papercraft pattern of [The Thinker][wiki-the-thinker] (originally, *Le Poète*), a bronze sculpture by [Auguste Rodin][wiki-rodin].

### Tools
* [Blender](https://www.blender.org)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
This pattern is based on 3D model [Lowest poly The Thinker](https://www.thingiverse.com/thing:2164071) by [LxO](https://www.thingiverse.com/LxO).

[wiki-the-thinker]: https://en.wikipedia.org/wiki/The_Thinker
[wiki-rodin]:  https://en.wikipedia.org/wiki/Auguste_Rodin

