---
layout: post
title:  "Papercraft asteroid Ryugu with place names"
date:   2019-04-18 07:00:00 +0200
tags: ryugu
categories: papercraft space
thumbnail: /files/2019-04-18/thumbnail.jpg
gallery:
  - name: Papercraft asteroid Ryugu photo 1
    file: /files/2019-04-18/Paper_Ryugu_1.jpg
  - name: Papercraft asteroid Ryugu photo 2
    file: /files/2019-04-18/Paper_Ryugu_2.jpg
  - name: Papercraft asteroid Ryugu photo 3
    file: /files/2019-04-18/Paper_Ryugu_3.jpg
  - name: Papercraft asteroid Ryugu English pattern
    file: /files/2019-04-18/Paper_Ryugu_English_Pattern.png
  - name: Papercraft asteroid Ryugu Japanese pattern
    file: /files/2019-04-18/Paper_Ryugu_Japanese_Pattern.png
---

{% include auto-gallery.html width="20" %}

### Download
* [Place names in English](/files/2019-04-18/Paper_Ryugu_English_v2.pdf) (3.6 MiB PDF file)
* [Place names in Japanese](/files/2019-04-18/Paper_Ryugu_Japanese_v2.pdf) (3.9 MiB PDF file)

### History
* 2019-06-28 - Compressed textures to make PDF file smaller.
* 2019-04-18 - Initial release.

### Dimentions
* Size: 9,0 cm x 9,0 cm x 9,0 cm
* Scale: 1/11,000

### Supplementary Data
Modified maps that account distortion around polar regions:
* [Map projection with English labels](/files/2019-04-18/Ryugu_Map_English.png) (1.1 MiB PNG file)
* [Map projection with Japanese labels](/files/2019-04-18/Ryugu_Map_Japanese.png) (1.4 MiB PNG file)

### Description
Papercraft polyhedral model of asteroid [Ryugu][wiki-ryugu] (162173 Ryugu, 1999 JU3) with the place names that were chosen by the [Hayabusa2][wiki-hayabusa2] team.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
Ryugu map by JAXA, University of Tokyo, Kochi University, Rikkyo University, Nagoya University, Chiba Institute of Technology, Meiji University, University of Aizu, AIST.
* [Source for the Japanese version](http://www.hayabusa2.jaxa.jp/topics/20190403_SCI_Schedule)
* [Source for the English version](http://www.hayabusa2.jaxa.jp/en/topics/20190403e_SCI_Schedule)

[wiki-hayabusa2]: https://en.wikipedia.org/wiki/Hayabusa2
[wiki-ryugu]:  https://en.wikipedia.org/wiki/162173_Ryugu
