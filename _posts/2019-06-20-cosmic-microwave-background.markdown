---
layout: post
title:  "Papercraft cosmic microwave background"
date:   2019-06-20 06:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-06-20/Thumbnail75x50_2.jpg
gallery:
  - name: Papercraft CMB Planck photo 1
    file: /files/2019-06-20/CMB_Planck_Photo_1.jpg
  - name: Papercraft CMB Planck photo 3
    file: /files/2019-06-20/CMB_Planck_Photo_3.jpg
  - name: Papercraft CMB Planck photo 4
    file: /files/2019-06-20/CMB_Planck_Photo_4.jpg
  - name: Papercraft CMB Planck large pattern
    file: /files/2019-06-20/CMB_Planck_Pattern_Large.png
  - name: Papercraft CMB Planck small pattern
    file: /files/2019-06-20/CMB_Planck_Pattern_Small.png
---

{% include auto-gallery.html width="20" %}

### Download
* [Large model consisting of two sheets](/files/2019-06-20/CMB_Planck_Large_v1.pdf) (2.5 MiB PDF file)
* [Small model consisting of a single sheet](/files/2019-06-20/CMB_Planck_Small_v1.pdf) (1.1 MiB PDF file)

### History
* 2019-06-20 - Initial release.

### Dimensions
* Large model - 11,0 cm x 11,0 cm x 11,0 cm
* Small model - 9,0 cm x 9,0 cm x 9,0 cm

### Description
[Cosmic background radiation][wiki-cmb] is a electromagnetic radiation that comes from all directions in the Universe. In 'Big Bang' cosmology this is a remnant from an early stage of the universe (hence the second term for it is 'relic radiation'). This all-sky survey was performed by the [Planck orbital observatory][wiki-planck].

Fluctuations in the background depict early universe since it is the oldest electromagnetic radiation. Thus, large red and blue chunks (areas with radiation rate higher and lower than average) are the remnants of the oldest structures in the universe.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Cosmic Microwave Background image by ESA/Planck Collaboration.
* [Planck all-sky images](http://planck.cf.ac.uk/all-sky-images)

[wiki-planck]: https://en.wikipedia.org/wiki/Planck_(spacecraft)
[wiki-cmb]: https://en.wikipedia.org/wiki/Cosmic_microwave_background
