---
layout: post
title:  "Papercraft comet 67P/C-G"
date:   2019-12-15 09:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-12-15/Thumbnail75x50.jpg
gallery:
  - name: Papercraft comet 67P/C-G photo 1
    file: /files/2019-12-15/67P_C-G_Photo_1.jpg
  - name: Papercraft comet 67P/C-G photo 2
    file: /files/2019-12-15/67P_C-G_Photo_2.jpg
  - name: Papercraft comet 67P/C-G photo 3
    file: /files/2019-12-15/67P_C-G_Photo_3.jpg
  - name: Papercraft comet 67P/C-G photo 4
    file: /files/2019-12-15/67P_C-G_Photo_4.jpg
  - name: Papercraft comet 67P/C-G photo 5
    file: /files/2019-12-15/67P_C-G_Photo_5.jpg
---

{% include auto-gallery.html width="20" %}

### Download
* [67P/C-G pattern](/files/2019-12-15/67P_C-G_Pattern_v1.pdf) (120.6 KiB PDF file)

### History
* 2019-12-15 - Initial release.

### Dimensions
* Size: 21,0 cm x 19,0 cm x 15,0 cm
* Scale: 1/20000

### Description
[67P/Churyumov-Gerasimenko][wiki-67P] (also abbreviated as 67P or 67P/C-G) is a short-period comet that completes one orbit around the Sun every 6,45 years. Originally from the Kuiper belt (region in the outer Solar system), it was a subject to gravitational perturbations from Jupiter and its orbit currently lies at distances between 1,24 AU and 3,64 AU from the Sun. The comet consists of two lobes connected by a narrower neck with its longest and widest dimensions of 4,3 by 4,1 km (2,7 by 2,7 miles).

67P was the destination of the international *[Rosetta][wiki-rosetta]* space probe. Launched in March 2004, *Rosetta* entered orbit of the comet in August-September 2014. *Rosetta*'s lander module, *[Philae][wiki-philae]* landed on the surface of the comet on 12 November 2014. *Rosetta* program ended on 30 September 2016 when the spacecraft was landed on the comet.

### Tools
* [Blender](https://www.blender.org)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
3D model of the 67P Comet was made using the images from NAVCAM, available under the Creative Commons license.
* [ESA/Rosetta/NAVCAM – CC BY-SA IGO 3.0](https://imagearchives.esac.esa.int/index.php?/category/56).

### See also
* [3D printable model of 67P on Thingiverse](https://www.thingiverse.com/thing:2979804)

[wiki-67P]: https://en.wikipedia.org/wiki/67P/Churyumov%E2%80%93Gerasimenko
[wiki-rosetta]: https://en.wikipedia.org/wiki/Rosetta_(spacecraft)
[wiki-philae]: https://en.wikipedia.org/wiki/Philae_(spacecraft)

