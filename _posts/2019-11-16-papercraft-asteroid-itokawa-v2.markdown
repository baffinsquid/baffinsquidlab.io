---
layout: post
title:  "Papercraft asteroid Itokawa v2"
date:   2019-11-16 14:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-11-16/Thumbnail75x50.jpg
gallery:
  - name: Papercraft asteroid Itokawa v2 photo 1
    file: /files/2019-11-16/Itokawa_v2_Photo_1.jpg
  - name: Papercraft asteroid Itokawa v2 photo 2
    file: /files/2019-11-16/Itokawa_v2_Photo_2.jpg
  - name: Papercraft asteroid Itokawa v2 photo 3
    file: /files/2019-11-16/Itokawa_v2_Photo_3.jpg
  - name: Papercraft asteroid Itokawa v2 photo 4
    file: /files/2019-11-16/Itokawa_v2_Photo_4.jpg
  - name: Papercraft asteroid Itokawa v2 photo 5
    file: /files/2019-11-16/Itokawa_v2_Photo_5.jpg
---

{% include auto-gallery.html width="20" %}

### Download
* [Itokawa v2 pattern](/files/2019-11-16/Itokawa_Huge_Patern_v1.pdf) (109.1 KiB PDF file)

### History
* 2019-11-16 - Initial version.

### Dimensions
* Size: 18,0 cm x 9,0 cm x 7,0 cm
* Scale: 1/3000

### Description
[Itokawa][wiki-itokawa] (25143 Itokawa, 1998 SF36) is a near-earth asteroid discovered in 1998 that is named after Japanese rocket engineer [Hideo Itokawa][wiki-hideo-itokawa]. In 2005 [Hayabusa spacecraft][wiki-hayabusa] explored this [S-type asteroid][wiki-s-type], and with the dimensions of approximately 330 meters, it is the smallest celestial body ever photographed or visited by an automatic probe as of 2019. In 2010, Hayabusa brought samples of dust and regolith from the asteroid's surface to the Earth for study of the evolution of the Solar System.

The pattern of this papercraft model is larger and more detailed than the [previous iteration](/papercraft/space/2019/03/31/papercraft-asteroid-itokawa), this time consisting of seven islands instead of the sole part. However, this version lacks mapped surface texture.

### Tools
* [Blender](https://www.blender.org)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)

### Credits
Asteroid model data by:
* Gaskell, R., Saito, J., Ishiguro, M., Kubota, T., Hashimoto, T., Hirata, N., Abe, S., Barnouin-Jha, O., and Scheeres, D., Gaskell Itokawa Shape Model V1.0. HAY-A-AMICA-5-ITOKAWASHAPE-V1.0. NASA Planetary Data System, 2008. [Available online](https://sbn.psi.edu/pds/resource/itokawashape.html)

### See also
* [Itokawa v1](/papercraft/space/2019/03/31/papercraft-asteroid-itokawa) - smaller version with mapped surface and only one paper island.
* [Itokawa](https://rightbasicbuilding.com/asteroid-maps/lpsc-handouts/itokawa/) and [Itokawa Cut ‘n’ Fold](https://rightbasicbuilding.com/asteroid-maps/itokawa-cut-n-fold/) by Chuck Clark.

[wiki-hayabusa]: https://en.wikipedia.org/wiki/Hayabusa
[wiki-itokawa]:  https://en.wikipedia.org/wiki/25143_Itokawa
[wiki-hideo-itokawa]:  https://en.wikipedia.org/wiki/Hideo_Itokawa
[wiki-s-type]:  https://en.wikipedia.org/wiki/S-type_asteroid

