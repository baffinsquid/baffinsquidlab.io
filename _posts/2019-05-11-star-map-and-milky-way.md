---
layout: post
title:  "Star map and the Milky Way polyhedron"
date:   2019-05-11 15:00:00 +0200
categories: papercraft space
thumbnail: /files/2019-05-11/Thumbnail75x50.jpg
gallery:
  - name: Papercraft star map photo 1
    file: /files/2019-05-11/Star_map_photo_1.jpg
  - name: Papercraft star map photo 2
    file: /files/2019-05-11/Star_map_photo_2.jpg
  - name: Papercraft star map photo 3
    file: /files/2019-05-11/Star_map_photo_3.jpg
  - name: Papercraft star map photo 4
    file: /files/2019-05-11/Star_map_photo_4.jpg
  - name: Papercraft star map pattern
    file: /files/2019-05-11/Star_map_Pattern.png
---

{% include auto-gallery.html width="20"  %}

### Download
* [Star map pattern](/files/2019-05-11/Star_map_v1.pdf) (18.9 MiB PDF file)

### History
* 2019-05-11 - Initial release.

### Dimensions
* Size: 11,0 cm x 11,0 cm x 11,0 cm

### Supplementary Data
Modified star map with coordinate grid:
* [Star map](/files/2019-05-11/Star_map.png) (4.8 MiB PNG file)

### Description
The star map in celestial coordinates with the constellation figures.

### Tools
* [Blender](https://www.blender.org)
* [GIMP2](https://www.gimp.org/)
* [Paper Model plug-in](https://github.com/addam/Export-Paper-Model-from-Blender)
* My [Autohedron scripts](https://baffinsquid.gitlab.io/papercraft/scripts/2019/05/26/autohedron-scripts) for converting geographic projection into a polyhedron

### Credits
Sky map by NASA/Goddard Space Flight Center Scientific Visualization Studio. Constellation figures based on those developed for the IAU by Alan MacRobert of Sky and Telescope magazine (Roger Sinnott and Rick Fienberg). 
* [Deep Star Maps](https://svs.gsfc.nasa.gov/3895)

